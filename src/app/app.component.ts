import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  response: any;
  show: boolean;
  constructor( private http:HttpClient) {
    this.show = false;
  }
  title = 'reel-download';
  onSubmit(data: string) {
    this.http.post('https://za58a3b9e-gtw.qovery.io/reel-download', data)
      .subscribe((result) => {
        console.warn(result);
        this.response = result;
        this.show = true;
      })
    console.warn(data);
  }
}
