const express = require("express");
const cors = require("cors");

const instareel = require("insta-reel");

const app = express();
const port = process.env.PORT || 3000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static('./dist/reel-download'));

app.get('/*', (req, res) =>
    res.sendFile('index.html', {root: 'dist/reel-download/'}),
);

app.post("/reel-download", async (req, res) => {
  try {
    const { url } = req.body;
    const reel = await instareel(url);
    res.status(200).json({
      success: true,
      message: "This is the reel video!",
      result: reel,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: "Not a valid URL!",
      error: error.message,
    });
  }
});

try {
  app.listen(port, () => {
    console.log(`Reel API listening on port ${port}!`);
  });
} catch (error) {
  console.log(error);
}
